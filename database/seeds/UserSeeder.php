<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
        	[
        		[
        			'name'			=> 'Rodel Hiolen',
        			'email'			=> 'rodel.hiolen@gmail.com',
        			'password'		=> bcrypt('123ewq!'),
        			'role'			=> 'admin'
        		],
                [
                    'name'          => 'Admin',
                    'email'         => 'admin@admin.com',
                    'password'      => bcrypt('123ewq!'),
                    'role'          => 'admin'
                ],
        		[
        			'name'			=> 'Test Marketer',
        			'email'			=> 'test@marketer.com',
        			'password'		=> bcrypt('123ewq!'),
        			'role'			=> 'marketer'
        		],
        		[
        			'name'			=> 'Test Researcher',
        			'email'			=> 'test@researcher.com',
        			'password'		=> bcrypt('123ewq!'),
        			'role'			=> 'researcher'
        		]
        	]
        );
    }
}
