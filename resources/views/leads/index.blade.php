<!-- resources/views/leads/index.blade.php -->

@extends('layouts.app')

@section('content') 
<!-- Flash Session -->
@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

@if(Session::has('error'))
    <div class="alert alert-danger">
        {{ Session::get('error') }}
    </div>
@endif
<div class="col-md-12">
    <h3>
        Data Leads
        @can('admin-access')
            <div class="pull-right">
                <a class="btn btn-sm  btn-warning iframe" href="{{ URL::to('lead/emailAll') }}">
                <span class="fa fa-envelope-o"></span> 
                Send All
                </a>
                <a class="btn btn-sm  btn-info iframe" href="{{ URL::to('lead/upload') }}" >
                <span class="fa fa-file-excel-o"></span> 
                Upload
                </a>
                <a class="btn btn-sm  btn-primary iframe" href="{{{ URL::to('lead/create') }}}" >
                <span class="glyphicon glyphicon-plus-sign"></span> 
                New
                </a>
            </div>
        @endcan
        @can('researcher-access')
            <div class="pull-right">
                <a class="btn btn-sm  btn-primary iframe" href="{{{ URL::to('lead/create') }}}" >
                <span class="glyphicon glyphicon-plus-sign"></span> 
                New
                </a>
            </div>
        @endcan
    </h3> 
    <table class="table" id="data-siswa">
        <thead>
            <tr>
                <td><strong>Name</strong></td>
                <td><strong>Email</strong></td>
                <td><strong>Website Url</strong></td>
                <td><strong>Uploaded By</strong></td>
                <td><strong>Created at</strong></td>
                @can('admin-access')
                <td><strong>Action</strong></td>
                @endcan
                @can('researcher-access')
                <td><strong>Action</strong></td>
                @endcan
            </tr>
        </thead>
    </table>
</div>
@endsection
 
{{-- Scripts --}}
@section('scripts')
    @parent
    <script type="text/javascript">
        var oTable;

        $(document).ready(function () {
            oTable = $('#data-siswa').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ URL::to('lead/data') }}",
                    
                "fnDrawCallback": function (oSettings) {
                    $(".iframe").colorbox({
                        iframe: true,
                        width: "80%",
                        height: "80%",
                        onClosed: function () {
                            window.location.reload();
                        }
                    });
                }
            });            
        });


    </script>
@endsection

