@extends('layouts.modal') @section('content')
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">Upload File</a></li>
</ul>

<form class="form-horizontal" id="fupload" enctype="multipart/form-data" method="post"
	action="{{ URL::to('lead/upload') }}" autocomplete="off">

	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
	
	<div class="form-group">
		<div class="col-lg-12">
			<label class="control-label" for="excel">Upload Excel</label> 
			<input name="excel" type="file" class="uploader" id="image" value="Upload" />
			<p class="help-block">.csv, .xlsx</p>
		</div>
	</div>

	<div class="form-group">
		<div class="controls">
			<button type="submit" class="btn btn-sm btn-success">
				<span class="fa fa-envelope-o"></span> Upload
			</button>
			<element class="btn btn-warning btn-sm close_popup">
				<span class="glyphicon glyphicon-ban-circle"></span> Cancel
			</element>
		</div>
	</div>
</form>
@stop
