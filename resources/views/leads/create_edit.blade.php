@extends('layouts.modal') @section('content')
<ul class="nav nav-tabs">
	<li class="active">
		<a href="#tab-create" data-toggle="tab">	 		
	 		@if (isset($lead)) 
			    Edit {{ $lead->name }}
			@else 
			    Create Leads
		     @endif
		</a>
	</li>
</ul>
<!-- Display Validation Errors -->
@include('common.errors')
<form class="form-horizontal" method="post"
	action="@if (isset($lead)){{ URL::to('lead/' . $lead->id . '/edit') }}@endif"
	autocomplete="off">
	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
	<div class="tab-content">
		<div class="tab-pane active" id="tab-general">
			<div class="col-md-12">
                <div class="form-group {{{ $errors->has('name') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="name">Username</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" tabindex="1"
                           	placeholder="username" name="name"
                           	id="name" value="{{{ Input::old('name', isset($lead) ? $lead->name : null) }}}" />
                          	{!! $errors->first('name', '<label class="control-label" for="username">:message</label>')!!}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="email">Email</label>
                    <div class="col-md-10">
                        <input class="form-control" type="email" tabindex="1"
                           	placeholder="example@email.com" name="email"
                           	id="email" value="{{{ Input::old('email', isset($lead) ? $lead->email : null) }}}" />
                          	{!! $errors->first('email', '<label class="control-label" for="email">:message</label>')!!}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('websiteUrl') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="websiteUrl">Website Url</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" tabindex="1"
                           	placeholder="www.example.com" name="websiteUrl"
                           	id="websiteUrl" value="{{{ Input::old('websiteUrl', isset($lead) ? $lead->websiteUrl : null) }}}" />
                          	{!! $errors->first('websiteUrl', '<label class="control-label" for="websiteUrl">:message</label>')!!}
                    </div>
                </div>
            </div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<button type="submit" class="btn btn-sm btn-success">
				<span class="glyphicon glyphicon-ok-circle"></span> Save
			</button>
			<button type="reset" class="btn btn-sm btn-warning close_popup">
				<span class="glyphicon glyphicon-ban-circle"></span> Cancel
			</button>
		</div>
	</div>
</form>
@stop
