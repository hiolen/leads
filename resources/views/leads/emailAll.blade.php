@extends('layouts.modal') @section('content')
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">Email All</a></li>
</ul>
<form id="deleteForm" class="form-horizontal" method="post" autocomplete="off"
	action="{{ URL::to('lead/emailAll') }}">

	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

	<div class="form-group">
		<div class="controls">
			Did you want to Email All this contact?<br>
			<button type="submit" class="btn btn-sm btn-success">
				<span class="fa fa-envelope-o"></span> Send All
			</button>
			<element class="btn btn-warning btn-sm close_popup">
				<span class="glyphicon glyphicon-ban-circle"></span> Cancel
			</element>
		</div>
	</div>
</form>
@stop
