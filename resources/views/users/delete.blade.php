@extends('layouts.modal') @section('content')
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">Delete {{$user->name}}</a></li>
</ul>
<form id="deleteForm" class="form-horizontal" method="post" autocomplete="off"
	action="@if (isset($user)){{ URL::to('user/' . $user->id . '/delete') }}@endif">

	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" /> <input
		type="hidden" name="id" value="{{ $user->id }}" />
	<div class="form-group">
		<div class="controls">
			Did you want to delete this user?<br>
			<button type="submit" class="btn btn-sm btn-danger">
				<span class="glyphicon glyphicon-trash"></span> Delete
			</button>
			<element class="btn btn-warning btn-sm close_popup">
				<span class="glyphicon glyphicon-ban-circle"></span> Cancel
			</element>
		</div>
	</div>
</form>
@stop
