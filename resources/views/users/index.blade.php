<!-- resources/views/leads/index.blade.php -->

@extends('layouts.app')

@section('content') 
<!-- Flash Session -->
@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

@if(Session::has('error'))
    <div class="alert alert-danger">
        {{ Session::get('error') }}
    </div>
@endif
<div class="col-md-12">
    <h3>
        Data Users
        <div class="pull-right">
            
            <a class="btn btn-sm  btn-primary iframe" href="{{{ URL::to('user/create') }}}" >
            <span class="glyphicon glyphicon-plus-sign"></span> 
            New
            </a>
        </div>
    </h3> 
    <table class="table" id="data-siswa">
        <thead>
            <tr>
                <td><strong>Name</strong></td>
                <td><strong>Email</strong></td>
                <td><strong>role</strong></td>
                <td><strong>Created at</strong></td>
                <td><strong>Action</strong></td>
            </tr>
        </thead>
    </table>
</div>
@endsection
 
{{-- Scripts --}}
@section('scripts')
    @parent
    <script type="text/javascript">
        var oTable;

        $(document).ready(function () {
            oTable = $('#data-siswa').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ URL::to('user/data') }}",
                    
                "fnDrawCallback": function (oSettings) {
                    $(".iframe").colorbox({
                        iframe: true,
                        width: "80%",
                        height: "80%",
                        onClosed: function () {
                            window.location.reload();
                        }
                    });
                }
            });            
        });


    </script>
@endsection

