@extends('layouts.modal') @section('content')
<ul class="nav nav-tabs">
	<li class="active">
		<a href="#tab-create" data-toggle="tab">	 		
	 		@if (isset($user)) 
			    Edit {{ $user->name }}
			@else 
		    Create User
	     @endif
		</a>
	</li>
</ul>
<!-- Display Validation Errors -->
@include('common.errors')
<form class="form-horizontal" method="post"
	action="@if (isset($user)){{ URL::to('user/' . $user->id . '/edit') }}@endif"
	autocomplete="off">
	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
	<div class="tab-content">
		<div class="tab-pane active" id="tab-general">
			<div class="col-md-12">
        <div class="form-group {{{ $errors->has('name') ? 'has-error' : '' }}}">
            <label class="col-md-2 control-label" for="name">Username</label>
            <div class="col-md-10">
              <input class="form-control" type="text" tabindex="1"
               	placeholder="username" name="name"
               	id="name" value="{{{ Input::old('name', isset($user) ? $user->name : null) }}}" />
              	{!! $errors->first('name', '<label class="control-label" for="name">:message</label>')!!}
            </div>
        </div>
        <div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
            <label class="col-md-2 control-label" for="email">Email</label>
            <div class="col-md-10">
              <input class="form-control" type="email" tabindex="1"
               	placeholder="example@email.com" name="email"
               	id="email" value="{{{ Input::old('email', isset($user) ? $user->email : null) }}}" />
              	{!! $errors->first('email', '<label class="control-label" for="email">:message</label>')!!}
            </div>
        </div>
        <div class="form-group {{{ $errors->has('password') ? 'has-error' : '' }}}">
            <label class="col-md-2 control-label" for="password">Password</label>
            <div class="col-md-10">
              <input class="form-control" type="password" tabindex="1"
                name="password" id="password" value="" />
                {!! $errors->first('password', '<label class="control-label" for="password">:message</label>')!!}
            </div>
        </div>
        <div class="form-group {{{ $errors->has('password_confirmation') ? 'has-error' : '' }}}">
            <label class="col-md-2 control-label" for="password_confirmation">Password Confirmation</label>
            <div class="col-md-10">
              <input class="form-control" type="password" tabindex="1"
                name="password_confirmation" id="password_confirmation" value="" />
                {!! $errors->first('password_confirmation', 
                '<label class="control-label" for="password_confirmation">:message</label>')!!}
            </div>
        </div>
        <div class="form-group {{{ $errors->has('role') ? 'has-error' : '' }}}">
            <label class="col-md-2 control-label" for="role">Role</label>
            <div class="col-md-10">
              <select style="width: 100%" name="role" id="role" class="form-control"> 
                @if (isset($user))
                  <option value="{{$user->role}}">{{$user->role}}</option>
                @else
                  <option value="researcher">Researcher</option>
                  <option value="marketer">Marketer</option>
                  <option value="admin">Admin</option>
                @endif
                
              </select>
            </div>
        </div>
      </div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<button type="submit" class="btn btn-sm btn-success">
				<span class="glyphicon glyphicon-ok-circle"></span> Save
			</button>
			<button type="reset" class="btn btn-sm btn-warning close_popup">
				<span class="glyphicon glyphicon-ban-circle"></span> Cancel
			</button>
		</div>
	</div>
</form>
@stop
