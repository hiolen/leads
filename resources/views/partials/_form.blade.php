<!-- Flash Session -->
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                {{ Session::get('flash_message') }}
            </div>
        @endif

        <div class="panel-body">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <!-- New Lead Form -->
            <form action="{{ url('lead') }}" method="POST" class="form-horizontal">
                {!! csrf_field() !!}

                <!-- Lead Name -->
                <div class="form-group">
                    <label for="lead-name" class="col-sm-3 control-label">Name:</label>
                    <div class="col-sm-6">
                        <input type="text" name="name" id="lead-name" class="form-control" placeholder="John Doe">
                    </div>
                </div>

                <!-- Lead Email   -->
                <div class="form-group">
                    <label for="lead-email" class="col-sm-3 control-label">Email:</label>
                    <div class="col-sm-6">
                        <input type="email" name="email" id="lead-email" class="form-control" placeholder="example@email.com">
                    </div>
                </div>

                <!-- Lead Website URL   -->
                <div class="form-group">
                    <label for="lead-websiteUrl" class="col-sm-3 control-label">Website url:</label>
                    <div class="col-sm-6">
                        <input type="text" name="websiteUrl" id="lead-websiteUrl" class="form-control" placeholder="http://mywebsite.com">
                    </div>
                </div>

                <!-- Lead Assign To   -->
                <div class="form-group">
                    <label for="lead-assignTo" class="col-sm-3 control-label">Assign To:</label>
                    <div class="col-sm-6">
                        <select name="assignedBy">
                            <option>Assign to...</option>
                            @foreach ($allUsers as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <!-- Add Lead Button -->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-plus"></i> Add Lead
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>