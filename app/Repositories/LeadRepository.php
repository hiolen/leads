<?php

namespace App\Repositories;

use App\Lead;

class LeadRepository
{
    /**
     * Get all of the leads for admin.
     *
     * @param  User  $user
     * @return Collection
     */
    public function getLeads()
    {
        return Lead::select(array('id', 'user_id', 'name', 'email', 'websiteUrl', 'sent_at', 'created_at'))
                ->orderBy('created_at', 'DESC')
                ->get();    
    }

    /**
     * Get all of the leads for admin.
     *
     * @return Collection
     */
    public function getDataLeads()
    {
        return Lead::join('users', 'users.id', '=', 'leads.user_id')
                ->select(array('leads.id', 'leads.user_id', 'leads.name', 'leads.email', 'leads.websiteUrl', 'users.name as uploaded_by', 'leads.sent_at', 'leads.created_at'))
                ->orderBy('created_at', 'DESC')
                ->get();
    }
}