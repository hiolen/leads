<?php

namespace App\Repositories;

use App\User;

class UserRepository
{
    /**
     * Get all of the leads for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function forUser(User $user)
    {
        return Lead::select(array('id', 'user_id', 'name', 'email', 'websiteUrl', 'created_at'))
                    ->where('user_id', $user->id)
                    ->orderBy('created_at', 'asc')
                    ->get();
    }

    /**
     * Get all of the leads for admin.
     *
     * @param  User  $user
     * @return Collection
     */
    public function getDataUsers()
    {
        return User::select(array('id', 'name', 'email', 'role', 'created_at'))
                ->orderBy('created_at', 'DESC')
                ->get();
    }

    /**
     * Get all of the leads for admin.
     *
     * @param  User  $user
     * @return Collection
     */
    public function getRole()
    {
        return User::select(array('role'))
                ->get();
    }
}