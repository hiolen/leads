<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('test', function () {
// 	try {
// 		$data = array(
//         'name' => "Learning Laravel",
// 	    );

// 	    Mail::send('emails.newsletter', $data, function ($message) {

// 	        $message->from('yourEmail@domain.com', 'Learning Laravel');

// 	        $message->to('rodel.hiolen@gmail.com')->subject('Learning Laravel test email');

// 	    });

// 	    return "Your email has been sent successfully";
// 	}catch (Exception $e) {
// 		return $e->getMessage();
// 	}
// });

Route::get('/', 'HomeController@index');

// Upload File
Route::get('lead/upload', 'LeadController@getFile');
Route::post('lead/upload', 'LeadController@postFile');

// Send Email
Route::get('lead/{lead}/email', 'LeadController@getEmail');
Route::post('lead/{lead}/email', 'LeadController@postEmail');

// Send All Email
Route::get('lead/emailAll', 'LeadController@getEmailAll');
Route::post('lead/emailAll', 'LeadController@postEmailAll');

// Send Add Email
Route::get('lead', 'LeadController@index');
Route::get('lead/create', 'LeadController@getCreate');
Route::post('lead/create', 'LeadController@postCreate');
Route::get('lead/{lead}/edit', 'LeadController@getEdit');
Route::post('lead/{lead}/edit', 'LeadController@postEdit');
Route::get('lead/{lead}/delete', 'LeadController@getDelete');
Route::post('lead/{lead}/delete', 'LeadController@postDelete');
Route::get('lead/data', 'LeadController@data');

Route::get('user', 'UserController@index');
Route::get('user/create', 'UserController@getCreate');
Route::post('user/create', 'UserController@postCreate');
Route::get('user/{user}/edit', 'UserController@getEdit');
Route::post('user/{user}/edit', 'UserController@postEdit');
Route::get('user/{user}/delete', 'UserController@getDelete');
Route::post('user/{user}/delete', 'UserController@postDelete');
Route::get('user/data', 'UserController@data');


Route::auth();



