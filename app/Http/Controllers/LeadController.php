<?php

namespace App\Http\Controllers;

use Session;
use Exception;
use Mail;

use App\Lead;
use App\User;

use Carbon\Carbon;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Repositories\LeadRepository;

use Datatables;

class LeadController extends Controller
{

	/**
     * The lead repository instance.
     *
     * @var LeadRepository
     */
    protected $leads;


    /**
     * Create a new controller instance.
     *
     * @param  LeadRepository $leads, LeadRepository $allLeads, LeadRepository $allUsers
     * @return void
     */
    public function __construct(LeadRepository $leads)
    {
        $this->middleware('auth');

        $this->leads = $leads;
    }

    /**
	 * Display a list of all of the user's lead.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function index(Request $request)
	{
	    return view('leads.index');
	}

	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        return view('leads.create_edit');
    }

    /**
	 * Create a new lead.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
    public function postCreate(Request $request) {

        $this->validate($request, [
		        'name' 			=> 'required|max:255',
		        'email' 		=> 'required|email|unique:leads',
		        'websiteUrl' 	=> 'required|max:255',
		    ]);

	    $request->user()->leads()->create([
	        'name' 		=> $request->name,
	        'email' 	=> $request->email,
	        'websiteUrl'=> $request->websiteUrl,
	        'sent_at' 	=> Carbon::now(),
	    ]);

	    Session::flash('flash_message', 'Lead Successfully added!');
    }
    
	/**
	 * Edit view of lead.
	 *
	 * @param  Lead  $lead
	 * @return Response
	 */
	public function getEdit(Lead $lead)
	{
	    return view('leads.create_edit', compact('lead'));
	}

	/**
	 * Edit lead.
	 *
	 * @param  Lead  $lead
	 * @return Response
	 */
	public function postEdit(Request $request, Lead $lead)
	{
	    $this->validate($request, [
	        'name' 			=> 'required|max:255',
	        'email' 		=> 'required|email',
	        'websiteUrl' 	=> 'required|max:255',
	    ]);

	    try {
			$this->authorize('update', $lead);	

		    $lead->update([
		        'name' 		=> $request->name,
		        'email' 	=> $request->email,
		        'websiteUrl'=> $request->websiteUrl,
		    ]);

		    Session::flash('flash_message', 'Lead Successfully Edited!');
		} catch (Exception $e) {
			Session::flash('error', 'You have no rights to edit this item!');
		}

	    
	}

	/**
	 * Get data to delete.
	 *
	 * @param  Request  $request
	 * @param  Lead  $lead
	 * @return Response
	 */
	public function getDelete(Lead $lead)
	{
	    return view('leads.delete', compact('lead'));
	}

	/**
	 * Destroy the given lead.
	 *
	 * @param  Request  $request
	 * @param  Lead  $lead
	 * @return Response
	 */
	public function postDelete(Lead $lead)
	{
		try {
			$this->authorize('destroyLead', $lead);	

			$lead->delete();

	    	Session::flash('flash_message', 'Lead Successfully deleted!');
		} catch (Exception $e) {
			Session::flash('error', 'You have no rights to delete this item!');
		}
	    

	    
	}

	/**
	 * get emailAll to lead
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function getEmailAll()
	{
	    return view('leads.emailAll');
	}

	/**
	 * Send All email to lead
	 * 
	 * @return Response
	 */
	public function postEmailAll()
	{
		$leads = $this->allLeads->getLeads();

		foreach ($leads as $lead) {
			try {
				$data = array(
			        'name' => $lead->name,
			        'email' => 'rodel.hiolen@gmail.com',
			        'website' => $lead->websiteUrl
			    );

				Mail::send('emails.newsletter', $data, function ($message) use ($lead) {

			        $message->from('rodel.hiolen@gmail.com', $lead->sent_at);

			        $message->to($lead->email)->subject('New Template')->bcc('rodel.hiolen@gmail.com');

				});

				$lead->update(['sent_at' => Carbon::now()]);

				Session::flash('flash_message', 'Your All email has been sent successfully');
			}catch (Exception $e) {
				Session::flash('error', $e->getMessage());
			}
			
		}

		

	}

	/**
	 * get email to lead
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function getEmail(Lead $lead)
	{
	    return view('leads.email', compact('lead'));
	}

	/**
	 * Send email to lead
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function postEmail(Lead $lead)
	{
		try {
			$data = array(
		        'name' => $lead->name,
		        'email' => 'rodel.hiolen@gmail.com',
		        'website' => $lead->websiteUrl
		    );

			Mail::send('emails.newsletter', $data, function ($message) use ($lead) {

		        $message->from('info@deesignerd.com', 'Deesignerd');

		        $message->to('rodel.hiolen@gmail.com')->subject('New Template')->bcc('rodel.hiolen@gmail.com');

			});

			$lead->update(['sent_at' => Carbon::now()]);

			Session::flash('flash_message', 'Your email has been sent successfully');
		}catch (Exception $e) {
			Session::flash('error', $e->getMessage());
		}
		

	}

	/**
	 * get file to lead
	 *
	 * @return Response
	 */
	public function getFile()
	{
	    return view('leads.upload');
	}

	/**
	 * Upload File to lead
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function postFile(Request $request)
	{
		// $this->validate($request, [
	 //        'excel' 	=> 'required|mimes:csv, xlsx',
	 //    ]);

        if($request->hasFile('excel'))
        {
        	try {
        		$file = $request->file('excel');
        		$data = Excel::load($file, function ($reader) {})->get();

				if(!empty($data) && $data->count()){
					foreach ($data as $key => $value) {
						$request->user()->leads()->create([
							'name' => trim((string)$value->author),
							'email' => (string)$value->email,
							'websiteUrl' => (string)$value->website
						]);
					}
				}

				Session::flash('flash_message', 'Lead Successfully Uploaded!');

        	} catch (Exception $e) {
        		Session::flash('error', $e->getMessage());
        	}
        	
        }
        
        
	}

	/**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
		$leads = $this->leads->getDataLeads();

        return Datatables::of($leads)
        	->edit_column('created_at', '{{ $created_at }}')
       		->add_column('actions', 
       			'@can("admin-access")
       				<a href="{{ url(\'lead/\'.$id.\'/email\') }}" class="btn btn-sm btn-warning iframe" data-toggle="tooltip" data-placement="left" title="sent at {{ $sent_at }}"><span class="fa fa-envelope-o"></span></a>
       				<a href="{{ url(\'lead/\'.$id.\'/edit\') }}" class="btn btn-sm btn-info iframe"><span class="glyphicon glyphicon-pencil"></span></a>
       				<a href="{{ url(\'lead/\'.$id.\'/delete\') }}" class="btn btn-sm btn-danger iframe"><span class="fa fa-trash"></span></a>
       				<script type="text/javascript">
					        $(function () {
					          $(\'[data-toggle="tooltip"]\').tooltip();
					        });
				    	</script>
       			@endcan
       			@can("researcher-access")
       				<a href="{{ url(\'lead/\'.$id.\'/edit\') }}" class="btn btn-sm btn-info iframe"><span class="glyphicon glyphicon-pencil"></span></a>
       			@endcan')
        	->remove_column('id', 'user_id', 'sent_at')
          ->make();
    }

}
