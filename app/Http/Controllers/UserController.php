<?php

namespace App\Http\Controllers;

use Session;
use Exception;
use App\User;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Datatables;

class UserController extends Controller
{
	/**
     * The lead repository instance.
     *
     * @var UserRepository
     */
    protected $users;


    /**
     * Create a new controller instance.
     *
     * @param  LeadRepository
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->middleware('auth');

        $this->users = $users;
    }

    /**
	 * Display a list of all of the user's.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function index(Request $request)
	{
	    return view('users.index');
	}

	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        return view('users.create_edit');
    }

    /**
	 * Create a new user.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
    public function postCreate(Request $request) {

        $this->validate($request, [
	        'name' 			=> 'required|max:255',
	        'email' 		=> 'required|email|unique:users',
            'password'      => 'required|confirmed|min:5'
	    ]);

	    $user = new User();
        $user -> name = $request->name;
        $user -> email = $request->email;
        $user -> password = bcrypt($request->password);
        $user -> role = $request->role;
        $user -> save();

	    Session::flash('flash_message', 'User Successfully added!');
    }

    /**
     * Edit view of User.
     *
     * @param  User  $user
     * @return Response
     */
    public function getEdit(User $user)
    {
        return view('users.create_edit', compact('user'));
    }

    /**
     * Edit User.
     *
     * @param  User  $user
     * @return Response
     */
    public function postEdit(Request $request, User $user)
    {
        $this->validate($request, [
            'name'          => 'required|max:255',
            'email'         => 'required|email',
            'password'      => 'required|confirmed|min:5'
        ]);

        try {
            $this->authorize('updateUser', $user);  

            $user->update([
                'name'      => $request->name,
                'email'     => $request->email,
                'websiteUrl'=> $request->websiteUrl,
            ]);

            Session::flash('flash_message', 'Lead Successfully Edited!');
        } catch (Exception $e) {
            Session::flash('error', 'You have no rights to edit this item!');
        }
    }  

    /**
     * Get data to delete.
     *
     * @param  Request  $request
     * @param  User  $user
     * @return Response
     */
    public function getDelete(User $user)
    {
        return view('users.delete', compact('user'));
    }

    /**
     * Destroy the given user.
     *
     * @param  Request  $request
     * @param  User  $user
     * @return Response
     */
    public function postDelete(User $user)
    {
        try {
            $this->authorize('destroyUser', $user); 

            $user->delete();

            Session::flash('flash_message', 'Lead Successfully deleted!');
        } catch (Exception $e) {
            // Session::flash('error', 'You have no rights to delete this item!');
            Session::flash('error', $e->getMessage());
        }
    }  

	/**
     * Show a list of all the users formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
		$users = $this->users->getDataUsers();

        return Datatables::of($users)
        	->add_column('actions', '
   				<a href="{{ url(\'user/\'.$id.\'/edit\') }}" class="btn btn-sm btn-info iframe"><span class="glyphicon glyphicon-pencil"></span></a>
   				<a href="{{ url(\'user/\'.$id.\'/delete\') }}" class="btn btn-sm btn-danger iframe"><span class="fa fa-trash"></span></a>')
        	->remove_column('id')
        	->make();
    }

}
