<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given lead can delete the given user.
     *
     * @param  User  $user
     * @return bool
     */
    public function destroyUser(User $user)
    {
        if ($user->role == 'admin')
            return true;
    }

    /**
     * Determine if the given lead can delete the given user.
     *
     * @param  User  $user
     * @return bool
     */
    public function updateUser(User $user)
    {
        if ($user->role == 'admin')
            return true;
    }
}
