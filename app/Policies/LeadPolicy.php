<?php

namespace App\Policies;

use App\User;
use App\Lead;
use Illuminate\Auth\Access\HandlesAuthorization;

class LeadPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given lead can delete the given user.
     *
     * @param  User  $user
     * @param  Lead  $lead
     * @return bool
     */
    public function destroyLead(User $user, Lead $lead)
    {
        if ($user->role == 'admin')
            return true;
        else
            return $user->id === $lead->user_id;
    }

    /**
     * Determine if the given lead can delete the given user.
     *
     * @param  User  $user
     * @param  Lead  $lead
     * @return bool
     */
    public function update(User $user, Lead $lead)
    {
        if ($user->role == 'admin')
            return true;
        else
            return $user->id === $lead->user_id;

    }
}
